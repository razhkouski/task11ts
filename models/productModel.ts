import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

interface IProduct extends mongoose.Document {
    name: string;
    price: number;
    category: string;
}

const productSchema = new mongoose.Schema({
    name: { type: String, require: true },
    price: { type: Number, default: 10 },
    category: { type: String, require: true }
}, { versionKey: false });

productSchema.plugin(autopopulate);
const Product = mongoose.model<IProduct>('product', productSchema);

export { Product, IProduct };