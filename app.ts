import express from 'express';
import * as mongoose from 'mongoose';
import { authRouter, categoryRouter, cartRouter, productRouter, orderRouter, userRouter } from './routes/index';
import { authJWT } from './controllers/authController';

const urlencodedParser = express.urlencoded({extended: true});
const app = express();

app.use("/auth", urlencodedParser, authRouter);
app.use("/user", authJWT, urlencodedParser, userRouter);
app.use("/category", authJWT, urlencodedParser, categoryRouter);
app.use("/cart", authJWT, urlencodedParser, cartRouter);
app.use("/product", authJWT, urlencodedParser, productRouter);
app.use("/order", authJWT, urlencodedParser, orderRouter);

mongoose.connect("mongodb://localhost:27017/BD", { useUnifiedTopology: true, useNewUrlParser: true }, function (err) {
    if (err) { return console.log('Something broke!'); }
    app.listen(3000, function () {
        console.log("Сервер работает");
    });
});