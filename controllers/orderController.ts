import { Category, ICategory } from '../models/categoryModel.js';
import { Product, IProduct } from '../models/productModel.js';
import { Request, Response } from 'express';

interface CustomRequest<T> extends Request {
    body: T
}

export const createProduct = async function (req: CustomRequest<IProduct>, res: Response) {
    try {
        let product = await Product.findOne({ name: req.body.name, price: req.body.price }),
            category = await Category.findOne({ categoryName: req.body.category });

        if (product) {
            return res.status(400).send('Такой товар уже существует.');
        } else if (!category) {
            return res.send('Указанная категория отсутствует.');
        } else {
            const newProduct = await new Product({ name: req.body.name, price: req.body.price, category: req.body.category });

            await newProduct.save();
            category.products.push(newProduct._id);
            category.populate('product');
            await category.save();

            return res.send(`Создан новый товар:\nназвание: ${newProduct.name},\nцена: ${newProduct.price} BYN.`);
        }
    } catch (e) {
        return res.status(400).send('Неверные параметры запроса.');
    }
};

export const deleteProduct = async (req: CustomRequest<IProduct>, res: Response) => {
    try {
        let product = await Product.findOne({ _id: req.body.id });

        if (!product) {
            return res.status(400).send('Не удалось найти корзину с таким id.');
        }
        let category = await Category.findOne({ categoryName: product.category });

        if (product) {
            Product.deleteOne({ _id: req.body.id });

            if (!category) {
                return res.status(400).send('Не удалось найти корзину с таким id.');
            }

            for (let i: any = 0; i < category.products.length; i++) {
                if (category.products[i]._id.toString() == product._id.toString()) {
                    category.products.splice(category.products.indexOf(i), 1);
                    await category.save();
                }
            }
            res.send(`Продукт ${product.name} удалён.`);
        } else {
            res.send('Не удалось найти товар с таким id.');
        }
    } catch (e) {
        res.status(400).send('Неверные параметры запроса. Не удалось удалить товар.');
    }
};

export const updateProduct = async (req: CustomRequest<IProduct>, res: Response) => {

    try {
        let product = await Product.findOne({ _id: req.body.id });
        if (!product) {
            return res.status(400).send('Не удалось найти корзину с таким id.');
        }

        if (product) {
            if (req.body.price && req.body.name) {

                Product.updateOne({ _id: req.body.id }, { $set: { price: req.body.price, name: req.body.name } }, { new: true }, function (err, result) {
                    if (err) return console.log(err);
                    return res.send(`Апдейт товара: ${product._id} \nНовое имя: ${req.body.name}\nНовая цена: ${req.body.price} BYN`);
                });
            } else if (req.body.name) {
                Product.updateOne({ _id: req.body.id }, { name: req.body.name }, { new: true }, function (err, result) {
                    if (err) return console.log(err);
                    return res.send(`Апдейт товара: ${product._id} \nНовое имя: ${req.body.name}`);
                });
            } else if (req.body.price) {
                Product.updateOne({ _id: req.body.id }, { price: req.body.price }, { new: true }, function (err, result) {
                    if (err) return console.log(err);
                    return res.send(`Апдейт товара: ${product._id} \nНовая цена: ${req.body.price} BYN`);
                });
            } else {
                return res.status(400).send('Указаны неверные параметры запроса');
            }
        } else {
            return res.send('Не удалось найти товар с таким id.');
        }
    } catch (e) {
        res.status(400).send('Неверные параметры запроса');
    }
};

export const getProductById = async (req: CustomRequest<IProduct>, res: Response) => {
    try {
        let product = await Product.findOne({ _id: req.body.id });

        return product ? res.send(`Наименование товара: ${product.name}, \nцена: ${product.price} BYN`)
            : res.send('Не удалось найти товар с таким id.');
    } catch (e) {
        res.status(400).send('Неверные параметры запроса.');
    }
};

export const getAllProducts = async function (req: CustomRequest<IProduct>, res: Response) {
    try {
        let products = await Product.find();

        return products ? res.send(`Список товаров: ${products}`) : res.send('Не удалось получить список товаров.');
    } catch (e) {
        res.status(400).send('Неверные параметры запроса');
    }
};