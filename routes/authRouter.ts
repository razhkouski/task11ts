import * as express from 'express';
import { login, registration, createGoogleUrl, getToken } from "../controllers/authController";
import userRouter from "./userRouter";

const authRouter: any = express.Router();

authRouter.post('/registration', registration);
authRouter.post('/login', login, userRouter);
authRouter.get("/google", createGoogleUrl);
authRouter.get("/google/getToken", getToken);

export default authRouter;