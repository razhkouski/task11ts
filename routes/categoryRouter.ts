import * as express from 'express';
import { checkRole } from '../controllers/authController';
import { createCategory, deleteCategory, updateCategory, getCategoryByID, getAllCategories } from "../controllers/categoryController";

const categoryRouter: any = express.Router();

categoryRouter.post("/create", checkRole, createCategory);
categoryRouter.delete("/delete", checkRole, deleteCategory);
categoryRouter.put("/update", checkRole, updateCategory);
categoryRouter.get("/getByID", getCategoryByID);
categoryRouter.get("/getAll", getAllCategories);

export default categoryRouter;