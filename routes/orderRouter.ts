import * as express from 'express';
import { createOrder, deleteOrder, getOrderById, getAllOrders } from '../controllers/orderController';

const orderRouter: any = express.Router();

orderRouter.post("/create", createOrder);
orderRouter.delete("/delete", deleteOrder);
orderRouter.get("/getOrderById", getOrderById);
orderRouter.get("/getAllOrders", getAllOrders);

export default orderRouter;