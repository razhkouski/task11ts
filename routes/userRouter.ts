import * as express from 'express';
import { createUser, deleteUser, updateUser, getUserByID, getAllUsers } from "../controllers/userController";
import { authJWT, checkRole } from '../controllers/authController';

const userRouter: any = express.Router();

userRouter.post("/create", authJWT, checkRole, createUser);
userRouter.delete("/delete", authJWT, checkRole, deleteUser);
userRouter.put("/update", authJWT, updateUser);
userRouter.get("/getByID", authJWT, getUserByID);
userRouter.get("/getAll", authJWT, getAllUsers);

export default userRouter;