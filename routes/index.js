import authRouter from "./authRouter";
import categoryRouter from "./categoryRouter";
import cartRouter from "./cartRouter";
import productRouter from "./productRouter";
import userRouter from "./userRouter";
import orderRouter from "./orderRouter";

export { authRouter, categoryRouter, cartRouter, productRouter, orderRouter, userRouter };